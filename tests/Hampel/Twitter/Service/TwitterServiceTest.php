<?php namespace Hampel\Twitter\Service;

use Guzzle\Tests\GuzzleTestCase;
use Guzzle\Service\Client;
use Hampel\Json\Json;

class TwitterServiceTest extends GuzzleTestCase
{
	public function setUp()
	{
		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testGet()
	{
		$client = new Client();
		$this->setMockResponse($client, 'rate_limit_application.json');

		$twitter = new TwitterService($client);
		$twitter->init();
		$response = $twitter->get('application/rate_limit_status', array(), array('resources' => 'application'));
		$response_json = $response->getBody(true);
		$data = Json::decode($response_json, true);

		$this->assertEquals(200, $response->getStatusCode());
		$this->assertArrayHasKey('resources', $data);
		$this->assertArrayHasKey('application', $data['resources']);
		$this->assertArrayHasKey('/application/rate_limit_status', $data['resources']['application']);
		$this->assertArrayHasKey('remaining', $data['resources']['application']['/application/rate_limit_status']);
		$this->assertEquals(178, $data['resources']['application']['/application/rate_limit_status']['remaining']);
	}

	public function testPost()
	{
		$client = new Client();
		$this->setMockResponse($client, 'user_lookup_twitterapi.json');

		$twitter = new TwitterService($client);
		$twitter->init();
		$response = $twitter->post('users/lookup', array(), array('screen_name' => 'twitterapi'), array());
		$response_json = $response->getBody(true);
		$data = Json::decode($response_json, true);

		$this->assertEquals(200, $response->getStatusCode());
		$this->assertArrayHasKey(0, $data);
		$this->assertArrayHasKey('id', $data[0]);
		$this->assertEquals(6253282, $data[0]['id']);
	}

	public function testGet400BadRequest()
	{
		$client = new Client();
		$this->setMockResponse($client, '400_bad_request_215.json');
		$this->setExpectedException('Hampel\Twitter\Service\TwitterException', 'Twitter API returned HTTP error Bad Request (code: 400) Twitter error: Bad Authentication data (code: 215)');

		$twitter = new TwitterService($client);
		$twitter->init();
		$response = $twitter->get('application/rate_limit_status', array(), array('resources' => 'application'));
	}

	public function testGet404NotFound()
	{
		$client = new Client();
		$this->setMockResponse($client, '404_not_found_34.json');
		$this->setExpectedException('Hampel\Twitter\Service\TwitterException', 'Twitter API returned HTTP error Not Found (code: 404) Twitter error: Sorry, that page does not exist (code: 34)');

		$twitter = new TwitterService($client);
		$twitter->init();
		$response = $twitter->get('application/rate_limit_status', array(), array('resources' => 'application'));
	}

	public function testGet429TooManyRequests()
	{
		$client = new Client();
		$this->setMockResponse($client, '429_too_many_requests_88.json');
		$this->setExpectedException('Hampel\Twitter\Service\TwitterException', 'Twitter API returned HTTP error Too Many Requests (code: 429) Twitter error: Rate limit exceeded (code: 88)');

		$twitter = new TwitterService($client);
		$twitter->init();
		$response = $twitter->get('application/rate_limit_status', array(), array('resources' => 'application'));
	}
}

?>