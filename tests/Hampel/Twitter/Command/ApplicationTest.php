<?php namespace Hampel\Twitter\Command;

use Hampel\Twitter\Service\TwitterService;
use Hampel\Twitter\Command\Application;
use Guzzle\Service\Client as Client;
use Guzzle\Tests\GuzzleTestCase;

class ApplicationTest extends GuzzleTestCase
{
	public function setUp()
	{
		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testGetRateLimit()
	{
		$client = new Client();
		$this->setMockResponse($client, 'rate_limit_application.json');

		$twitter = new TwitterService($client);
		$twitter->init();
		$application = new Application($twitter);
		$data = $application->getRateLimit('application');

		$this->assertArrayHasKey('/application/rate_limit_status', $data);

		$info = $data['/application/rate_limit_status']->get();
		$this->assertEquals('/application/rate_limit_status', $info['name']);
		$this->assertEquals(180, $info['limit']);
		$this->assertEquals(178, $info['remaining']);
		$this->assertEquals(1371450232, $info['reset']);
	}
}

?>