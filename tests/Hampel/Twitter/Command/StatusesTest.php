<?php namespace Hampel\Twitter\Command;

use Hampel\Twitter\Service\TwitterService;
use Hampel\Twitter\Command\Statuses;
use Hampel\Twitter\Response\Status;
use Hampel\Twitter\Response\Url;
use Guzzle\Service\Client;
use Guzzle\Tests\GuzzleTestCase;

class StatusesTest extends GuzzleTestCase
{
	public function setUp()
	{
		date_default_timezone_set('UTC');

		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testGetUserTimeline()
	{
		$client = new Client();
		$this->setMockResponse($client, array('rate_limit_statuses.json', 'user_timeline_twitterapi.json'));

		$twitter = new TwitterService($client);
		$twitter->init();
		$statuses = new Statuses($twitter);
		$data = $statuses->getUserTimeline(0, "twitterapi", "", "", 2);

		$this->assertEquals(2, count($data));
		$this->assertArrayHasKey('344581518833905664', $data);
		$this->assertTrue($data['344581518833905664'] instanceof Status);
		$this->assertArrayHasKey('344574437674328064', $data);
		$this->assertTrue($data['344574437674328064'] instanceof Status);
	}

	public function testGetUserTimelineAlmostLimited()
	{
		$client = new Client();
		$this->setMockResponse($client, array('rate_limit_statuses_almost_limited.json', 'user_timeline_twitterapi.json'));
		$this->setExpectedException('Hampel\Twitter\Service\TwitterException');

		$twitter = new TwitterService($client);
		$twitter->init();
		$statuses = new Statuses($twitter);
		$data = $statuses->getUserTimeline(0, "twitterapi", "", "", 2);
	}

	public function testGetEarliestStatusId()
	{
		$client = new Client();
		$this->setMockResponse($client, array(
			'rate_limit_statuses.json',
			'earliest_status_twitterapi_1.json',
			'earliest_status_twitterapi_2.json',
			'earliest_status_twitterapi_3.json',
			'earliest_status_twitterapi_done.json',
		));

		$twitter = new TwitterService($client);
		$twitter->init();
		$statuses = new Statuses($twitter);
		$data = $statuses->getEarliestStatusId(0, "twitterapi");

		$this->assertEquals('75979616254308353', $data);
	}

	public function testGetAllStatuses()
	{
		$client = new Client();
		$this->setMockResponse($client, array(
			'rate_limit_statuses.json',
			'earliest_status_twitterapi_1.json',
			'earliest_status_twitterapi_2.json',
			'earliest_status_twitterapi_3.json',
			'earliest_status_twitterapi_done.json',
		));

		$twitter = new TwitterService($client);
		$twitter->init();
		$statuses = new Statuses($twitter);
		$data = $statuses->getAllStatuses(0, "twitterapi");

		$this->assertEquals('599', count($data));
		$this->assertArrayHasKey('344581518833905664', $data);
		$this->assertArrayHasKey('75979616254308353', $data);
	}

}

?>