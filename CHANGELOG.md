CHANGELOG
=========

0.4.1 (2013-07-10)
------------------

* fixed array_merge bug - array keys get renumbered, so replaced with array addition (+)
* added missing RateLimit::getName() function
* added array key checks to RateLimit::getName() and RateLimit::getRemaining() checks

0.4.0 (2013-06-22)
------------------

* refactored Response classes to store data as name-value pairs rather than class properties
* Response classes now filter out any complex data types (arrays, objects, etc)
* new Response parent class from which all other "Response" classes are now extended

0.3.0 (2013-06-19)
------------------

* getLookupUser is now getShowUser (returns single user object via users/show API call)
* implemented postLookupUsersByUserId and postLookupUsersByScreenName functions (users/lookup API call)
* implemented POST call for Guzzle
* additional rate limit caching
* refactored network group unit tests to call TwitterService directly rather than via helper functions
* lots of new unit tests

0.2.0 (2013-06-18)
----------------

* implemented getEarliestStatusId function in Hampel\Twitter\Command\Statuses
* implemented getAllStatuses function in Hampel\Twitter\Command\Statuses
* refactored code to remove OauthPlugin dependency to make unit testing easier
* added getLastStatusCode function to Hampel\Twitter\Service\TwitterService
* lots of new unit tests
* implemented bcmath for string based arithmetic (eg on status_id) for systems that only support 32-bit integers
* added rate limit caching

0.1.0 (2013-06-17)
------------------

* initial release
* implemented calls for application/rate_limit_status, statuses/user_timeline and users/lookup