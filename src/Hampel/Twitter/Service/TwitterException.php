<?php namespace Hampel\Twitter\Service;

use Hampel\Json\Json;
use Hampel\Json\JsonException;

/**
 * Custom Twitter Exception which provides additional information for Twitter errors
 *
 */
class TwitterException extends \Exception
{
	/** @var array extra information about twitter errors. Not currently used! */
	protected $twitter_errors = array(
		32 => array('text' => "Could not authenticate you", 'description' => "Your call could not be completed as dialed"),
		34 => array('text' => "Sorry, that page does not exist", 'description' => "Corresponds with an HTTP 404 - the specified resource was not found"),
		64 => array('text' => "Your account is suspended and is not permitted to access this feature", 'description' => "Corresponds with an HTTP 403 - the access token being used belongs to a suspended user and they can't complete the action you're trying to take"),
		68 => array('text' => "The Twitter REST API v1 is no longer active. Please migrate to API v1.1", 'description' => "Corresponds to a HTTP request to a retired v1-era URL"),
		88 => array('text' => "Rate limit exceeded", 'description' => "The request limit for this resource has been reached for the current rate limit window"),
		89 => array('text' => "Invalid of expired token", 'description' => "The access token used in the request is incorrect or has expired"),
		130 => array('text' => "Over capacity", 'description' => "Corresponds with an HTTP 503 - Twitter is temporarily over capacity"),
		131 => array('text' => "Internal error", 'description' => "Corresponds with an HTTP 500 - An unknown internal error occurred"),
		135 => array('text' => "Could not authenticate you", 'description' => "Corresponds with a HTTP 401 - it means that your oauth_timestamp is either ahead or behind our acceptable range"),
		187 => array('text' => "Status is a duplicate", 'description' => "The status text has been Tweeted already by the authenticated account"),
		215 => array('text' => "Bad authentication data", 'description' => "Typically sent with 1.1 responses with HTTP code 400. The method requires authentication but it was not presented or was wholly invalid"),
		231 => array('text' => "User must verify login", 'description' => "Returned as a challenge in xAuth when the user has login verification enabled on their account and needs to be directed to twitter.com to generate a temporary password")
	);

	/**
	 * Custom constructor
	 *
	 * @param string $message		the message that corresponds to this error. Additional information may be appended to this message
	 * @param number $code			the numeric code associated with this error (if any)
	 * @param string $twitter_error	the twitter error JSON returned by Twitter API call for non 200 status codes
	 */
	public function __construct($message, $code = null, $twitter_error = "")
	{
		if (!is_null($code)) $message = "Twitter API returned HTTP error {$message} (code: {$code})";

		if (!empty($twitter_error))
		{
			try {
				$error = Json::decode($twitter_error, true);

				if (!empty($error))
				{
					$code = $error['errors'][0]['code'];
					$message .= " Twitter error: {$error['errors'][0]['message']} (code: {$code})";
				}
			}
			catch (JsonException $e) {
				$message .= " (error decoding Twitter Error JSON: " . $e->getMessage() . ")";
			}
		}

		parent::__construct($message, $code);
	}
}

?>