<?php namespace Hampel\Twitter\Service;

use Hampel\Twitter\Response\RateLimit;
use Guzzle\Http\Client;
use Guzzle\Plugin\Oauth\OauthPlugin;

/**
 * The main service interface using Guzzle
 *
 */
class TwitterService
{
	/** @var string version information for API calls */
	protected $version = "1.1";

	/** @var TwitterConfig our config object */
	protected $config;

	/** @var Client our Guzzle HTTP Client object */
	protected $client;

	/** @var OauthPlugin Guzzle OauthPlugin object */
	protected $oauth;

	/** @var Response Guzzle Response object representing the last response from Guzzle call to Twitter API */
	protected $last_response;

	/** @var array Rate limit cache array */
	protected $rate_limit_cache = array();

	/**
	 * Constructor
	 *
	 * @param Client $client		Guzzle HTTP client
	 * @param OauthPlugin $oauth	Guzzle OauthPlugin object
	 * @param TwitterConfig $config	Config object
	 */
	public function __construct(Client $client, OauthPlugin $oauth = null, TwitterConfig $config = null)
	{
		$this->client = $client;
		$this->oauth = $oauth;
		$this->config = $config;
	}

	/**
	 * Init - must be called after construction and before any other actions on this class
	 */
	public function init()
	{
		$this->client->setBaseUrl('https://api.twitter.com/' . $this->version);
		if (!is_null($this->oauth))	$this->client->addSubscriber($this->oauth);
	}

	/**
	 * Make a GET call to the API via Guzzle
	 *
	 * @param string $command	Twitter command to send
	 * @param array $headers	array of header information
	 * @param array $options	array of options, including API call parameters
	 *
	 * @throws TwitterException
	 *
	 * @return Response Guzzle Response object
	 */
	public function get($command, array $headers, array $options)
	{
		if (!isset($options['exceptions']))	$options['exceptions'] = false;

		$request = $this->client->get("{$command}.json", $headers, $options);
		$response = $this->client->send($request);

		if (!$response->isSuccessful()) throw new TwitterException($response->getReasonPhrase(), $response->getStatusCode(), $response->getBody());

		$this->last_response = $response;
		$rate_limit_remaining = $response->getHeader('x-rate-limit-remaining');
		if (!is_null($rate_limit_remaining)) $this->rate_limit_cache[$command] = $rate_limit_remaining->__toString();

		return $response;
	}

	/**
	 * Make a POST call to the API via Guzzle
	 *
	 * @param string $command	Twitter command to send
	 * @param array $headers	array of header information
	 * @param array $postbody	associative array of post fields
	 * @param array $options	array of options
	 *
	 * @throws TwitterException
	 *
	 * @return Response Guzzle Response object
	 */
	public function post($command, array $headers, array $postbody, array $options)
	{
		if (!isset($options['exceptions']))	$options['exceptions'] = false;

		$request = $this->client->post("{$command}.json", $headers, $postbody, $options);
		$response = $this->client->send($request);

		if (!$response->isSuccessful()) throw new TwitterException($response->getReasonPhrase(), $response->getStatusCode(), $response->getBody());

		$this->last_response = $response;
		$rate_limit_remaining = $response->getHeader('x-rate-limit-remaining');
		if (!is_null($rate_limit_remaining)) $this->rate_limit_cache[$command] = $rate_limit_remaining->__toString();

		return $response;
	}

	/**
	 * Return the status code from the last API call made
	 *
	 * @return number status code
	 */
	public function getLastStatusCode()
	{
		return $this->last_response->getStatusCode();
	}

	/**
	 * Return the response object from the last API call made
	 *
	 * @return Response Guzzle Reponse object
	 */
	public function getLastResponse()
	{
		return $this->last_response;
	}

	/**
	 * Check the cache for rate limit information about a resource and return it if available
	 *
	 * @param string $resource	name of the resource
	 * @return RateLimit or false if not available
	 */
	public function getRateLimitFromCache($resource)
	{
		if (array_key_exists($resource, $this->rate_limit_cache)) return $this->rate_limit_cache[$resource];
		else return false;
	}

	/**
	 * Update the rate limit cache with information returned from application/rate_limit_status call
	 *
	 * @param string $json	data from application/rate_limit_status
	 */
	public function updateRateLimitCache($json)
	{
		$rate_limits = RateLimit::extractAllRateLimits($json);
		foreach ($rate_limits as $name => $rate_limit)
		{
			$this->rate_limit_cache[$name] = $rate_limit->getRemaining();
		}
	}

	/**
	 * Get rate limit cache array (mostly for testing purposes)
	 *
	 * @return array of RateLimit
	 */
	public function getRateLimitCache()
	{
		return $this->rate_limit_cache;
	}
}

?>