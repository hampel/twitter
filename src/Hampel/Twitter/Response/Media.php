<?php namespace Hampel\Twitter\Response;

use Hampel\Json\Json;
use Hampel\Twitter\Service\TwitterException;

/**
 * Represents a Media entity
 *
 */
class Media extends Entity
{
	/** @var string type of entity for matching purposes */
	protected $type = "media";

	/** @var array an array of size objects */
	protected $sizes;

	/**
	 * Sets data to the object
	 *
	 * @param string $status_id		status_id of the status this entity belongs to
	 * @param array $data			data array from decoded JSON
	 */
	public function set(array $data)
	{
		if (!isset($data['type'])) throw new TwitterException("Invalid data received - no type found in media");

		$data['media_type'] = $data['type'];
		unset($data['type']);

		$this->sizes = Size::extractSizes($data['sizes']);
		unset($data['sizes']);

		if (!isset($data['id'])) throw new TwitterException("Invalid data received - no id found in media");
		if (!isset($data['id_str'])) throw new TwitterException("Invalid data received - no id_str found in media");

		$data['media_id'] = $data['id_str'];
		unset($data['id'], $data['id_str']);

		if (isset($data['source_status_id'])) unset($data['source_status_id']);
		if (isset($data['source_status_id_str']))
		{
			$data['source_status_id'] = $data['source_status_id_str'];
			unset($data['source_status_id_str']);
		}

		parent::set($data);
	}

	/**
	 * Get the sizes array
	 *
	 * @return array size name-value pairs
	 */
	public function getSizes()
	{
		return $this->sizes;
	}

	/**
	 * Get the sizes array encoded in JSON
	 *
	 * @return string JSON encoded data
	 */
	public function getSizesJson()
	{
		return Json::encode($this->sizes);
	}
}

?>