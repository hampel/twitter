<?php namespace Hampel\Twitter\Response;

use Hampel\Twitter\Service\TwitterException;

/**
 * Represents size data for media entites
 *
 */
class Size extends Response
{
	/**
	 * Sets data to the object
	 *
	 * @param array $data			data array from decoded JSON
	 */
	public function set(array $data)
	{
		if (!isset($data['h'])) throw new TwitterException("Invalid data received - no height found in size");
		if (!isset($data['w'])) throw new TwitterException("Invalid data received - no width found in size");

		$data['height'] = $data['h'];
		$data['width'] = $data['w'];

		parent::set($data);
	}

	/**
	 * Extract size data from the decoded JSON array
	 *
	 * @param array $sizes		size data from decoded JSON
	 * @return array of size objects
	 */
	public static function extractSizes(array $sizes)
	{
		$size_array = array();

		if (empty($sizes)) return $size_array;

		foreach ($sizes as $name => $data)
		{
			$size = new Size();
			$size->set($data);
			$size_array[$name] = $size;
		}

		return $size_array;
	}
}

?>