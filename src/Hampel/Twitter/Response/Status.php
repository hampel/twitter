<?php namespace Hampel\Twitter\Response;

use Hampel\Json\Json;
use Hampel\Twitter\Service\TwitterException;

/**
 * Represents a status update
 *
 */
class Status extends Response
{

	/** @var array array of entity objects for the status */
	protected $entities;

	/**
	 * Sets data to the object
	 *
	 * @param array $data	status data array from decoded JSON
	 */
	public function set(array $data)
	{
		if (!isset($data['id'])) throw new TwitterException("Invalid data received - no id found in status");
		if (!isset($data['id_str'])) throw new TwitterException("Invalid data received - no id_str found in status");

		$data['status_id'] = $data['id_str'];
		unset($data['id'], $data['id_str']);

		$data['created_at'] = strtotime($data['created_at']);

		if (isset($data['entities']))
		{
			$this->entities = Entity::extractEntities($data['entities'], $data['status_id']);
			unset($data['entities']);
		}

		if (isset($data['in_reply_to_status_id'])) unset($data['in_reply_to_status_id']);
		if (isset($data['in_reply_to_status_id_str']))
		{
			$data['in_reply_to_status_id'] = $data['in_reply_to_status_id_str'];
			unset($data['in_reply_to_status_id_str']);
		}

		if (isset($data['in_reply_to_user_id'])) unset($data['in_reply_to_user_id']);
		if (isset($data['in_reply_to_user_id_str']))
		{
			$data['in_reply_to_user_id'] = $data['in_reply_to_user_id_str'];
			unset($data['in_reply_to_user_id_str']);
		}

		// strip off some other object or array data we won't currently use
		if (isset($data['annotations'])) unset($data['annotations']);
		if (isset($data['contributors'])) unset($data['contributors']);
		if (isset($data['coordinates'])) unset($data['coordinates']);
		if (isset($data['current_user_retweet'])) unset($data['current_user_retweet']);
		if (isset($data['geo'])) unset($data['geo']);
		if (isset($data['lang'])) unset($data['lang']);
		if (isset($data['place'])) unset($data['place']);
		if (isset($data['scopes'])) unset($data['scopes']);
		if (isset($data['user'])) unset($data['user']);
		if (isset($data['withheld_in_countries'])) unset($data['withheld_in_countries']);

		parent::set($data);
	}

	/**
	 * Build an array of Status objects returned by a Twitter API call
	 *
	 * @param string $json		JSON data returned by Twitter
	 * @param string $user_id	user_id of the user who posted this update
	 *
	 * @return array of User objects
	 */
	public static function extractStatuses($json, $user_id)
	{
		$status_array = array();

		if (empty($json)) return $status_array;

		$data = Json::decode($json, true);

		foreach ($data as $status_info)
		{
			$status = new Status();
			$status_info['user_id'] = $user_id;
			$status->set($status_info);
			$status_array[$status_info['id_str']] = $status;
		}

		return $status_array;
	}

	/**
	 * Return the status_id of this status
	 *
	 * @return string status_id
	 */
	public function getStatusId()
	{
		return $this->data['status_id'];
	}

	/**
	 * Get the entity array
	 *
	 * @return array entity name-value pairs
	 */
	public function getEntities()
	{
		return $this->entities;
	}

}

?>