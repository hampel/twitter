<?php namespace Hampel\Twitter\Command;

use Hampel\Twitter\Service\TwitterService;
use Hampel\Twitter\Service\TwitterException;

/**
 * Family abstract class - provides common functionality for API wrapper calls
 *
 */
abstract class Family
{
	/** @var string Prefix for commands */
	protected $prefix;

	/** @var TwitterService Our Twitter service */
	protected $twitter;

	/**
	 * Constructor
	 *
	 * @param TwitterService $twitter
	 */
	public function __construct(TwitterService $twitter)
	{
		$this->twitter = $twitter;
	}

	/**
	 * Is this resource rate limited?
	 *
	 * Returns FALSE if not rate limited, otherwise, returns the rate limit object for more information if remaining rate limit less than or equal to $min_rate_limit
	 *
	 * @param string $resource Name of the Twitter resource to check (eg "statuses/user_timeline") see: https://dev.twitter.com/docs/rate-limiting/1.1/limits
	 * @param number $min_rate_limit minimum rate limit to be considered "rate limited"
	 *
	 * @throws TwitterException
	 *
	 * @return bool OR \Hampel\Twitter\Response\RateLimit
	 */
	public function isRateLimited($resource, $min_rate_limit = 5)
	{
		// check cache first
		$remaining = $this->twitter->getRateLimitFromCache($resource);

		if ($remaining === false)
		{
			$application = new Application($this->twitter);

			$ratelimit = $application->getRateLimit($this->prefix, $resource);
			if (is_null($ratelimit)) throw new TwitterException("Could not find rate limit resource {$resource}");

			$remaining = $ratelimit->getRemaining();
		}

		if ($remaining <= $min_rate_limit) return $ratelimit;
		else return false;
	}
}

?>