<?php namespace Hampel\Twitter\Command;

use Hampel\Twitter\Response\User;
use Hampel\Twitter\Service\TwitterException;

/**
 * Users Twitter API group
 *
 */
class Users extends Family
{
	/** @var string Prefix for commands */
	protected $prefix = 'users';

	/**
	 * Wrapper for users/show API call
	 *
	 * @param string $user_id		Twitter user_id to get timeline for. Use this rather than screen_name if available.
	 * @param string $screen_name	Twitter screen_name to get timeline for. If both user_id and screen_name supplied, user_id will be used.
	 *
	 * @throws TwitterException
	 *
	 * @return mixed array of User objects
	 */
	public function getShowUser($user_id, $screen_name = "")
	{
		$command = $this->prefix . '/show';

		$rateLimited = $this->isRateLimited("{$command}/:id");
		if ($rateLimited !== false) throw new TwitterException("About to get rate limited - " . $rateLimited->toString());

		$headers = array();
		$options = array('query' => array());

		if (!empty($user_id)) $options['query']['user_id'] = $user_id;
		else $options['query']['screen_name'] = $screen_name;

		$response = $this->twitter->get($command, $headers, $options);

		$response_json = $response->getBody(true);
		if (empty($response_json)) throw new TwitterException("Empty body received from {$command}");

		return User::extractUser($response_json);
	}

	/**
	 * Wrapper for users/lookup API call (using POST)
	 *
	 * @param array $userids	array of Twitter user_id
	 *
	 * @throws TwitterException
	 *
	 * @return array of User objects
	 */
	public function postLookupUsersByUserId(array $userids)
	{
		if (empty($userids)) return array();

		$users_array = array();

		$command = $this->prefix . '/lookup';

		$rateLimited = $this->isRateLimited($command);
		if ($rateLimited !== false) throw new TwitterException("About to get rate limited - " . $rateLimited->toString());

		$headers = array();
		$options = array();

		$userid_chunks = array_chunk($userids, 100);
		foreach ($userid_chunks as $chunk)
		{
			$userid_list = implode(',', $chunk);

			$postbody['user_id'] = $userid_list;

			$response = $this->twitter->post($command, $headers, $postbody, $options);

			$response_json = $response->getBody(true);
			if (empty($response_json)) throw new TwitterException("Empty body received from {$command}");

			$users_array = array_merge($users_array, User::extractUsers($response_json));
		}

		return $users_array;
	}

	/**
	 * Wrapper for users/lookup API call (using POST)
	 *
	 * @param array $screen_names	array of Twitter screen_name
	 *
	 * @throws TwitterException
	 *
	 * @return array of User objects
	 */
	public function postLookupUsersByScreenName(array $screen_names)
	{
		if (empty($screen_names)) return array();

		$users_array = array();

		$command = $this->prefix . '/lookup';

		$rateLimited = $this->isRateLimited($command);
		if ($rateLimited !== false) throw new TwitterException("About to get rate limited - " . $rateLimited->toString());

		$headers = array();
		$options = array();

		$screen_name_chunks = array_chunk($screen_names, 100);
		foreach ($screen_name_chunks as $chunk)
		{
			$screen_name_list = implode(',', $chunk);

			$postbody['screen_name'] = $screen_name_list;

			$response = $this->twitter->post($command, $headers, $postbody, $options);

			$response_json = $response->getBody(true);
			if (empty($response_json)) throw new TwitterException("Empty body received from {$command}");

			$users_array = array_merge($users_array, User::extractUsers($response_json));
		}

		return $users_array;
	}

}

?>