<?php namespace Hampel\Twitter\Command;

use Hampel\Twitter\Response\RateLimit;
use Hampel\Twitter\Service\TwitterException;

/**
 * Application Twitter API group
 *
 */
class Application extends Family
{
	/** @var string Prefix for commands */
	protected $prefix = 'application';

	/**
	 * application/rate_limit_status
	 *
	 * @param mixed $family 	String representing resource family (prefix), or an array of strings
	 * @param string $resource 	Optional single resource to return data for (eg "statuses/user_timeline")
	 *
	 * @throws TwitterException
	 *
	 * @return array of RateLimit objects or single rate limit object
	 */
	public function getRateLimit($family, $resource = "")
	{
		$command = $this->prefix . '/rate_limit_status';
		$headers = array();
		$options = array();

		if (is_array($family)) $resource_list = implode(',', $family);
		else $resource_list = $family;

		if ($resource_list) $options['query'] = array('resources' => $resource_list);

		$response = $this->twitter->get($command, $headers, $options);

		$response_json = $response->getBody(true);
		if (empty($response_json)) throw new TwitterException("Empty body received from {$command}");

		$this->twitter->updateRateLimitCache($response_json);

		if ($resource) return RateLimit::extractSingleRateLimit($response_json, "/{$resource}");
		else return RateLimit::extractAllRateLimits($response_json);
	}
}

?>