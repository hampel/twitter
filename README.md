Hampel Twitter
==============

A Twitter API wrapper using Guzzle

By [Simon Hampel](http://hampelgroup.com/).

Installation
------------

The recommended way of installing Hampel Twitter is through [Composer](http://getcomposer.org):

    {
        "require": {
            "hampel/twitter": "dev-master"
        }
    }
    
Usage
-----

    <?php

	use Hampel\Twitter\Service\TwitterConfig;
	use Hampel\Twitter\Service\TwitterService;
	use Hampel\Twitter\Command\Statuses;

	$config = new TwitterConfig();
	$config->set("consumer key", "consumer secret", "token ", "token secret");
	$oauth = new Guzzle\Plugin\Oauth\OauthPlugin($config->getOauthParamArray());
	$client = new Guzzle\Http\Client();
	$twitter = new TwitterService($client, $oauth);
	$twitter->init();	

	$application = new Application($twitter);
	$data = $application->getRateLimit('application');
	var_dump($data);
	
	$statuses = new Statuses($twitter);
	$data = $statuses->getUserTimeline(0, "twitterapi", "", 2);
	var_dump($data);
	$data = $statuses->getEarliestStatusId(0, "twitterapi");
	var_dump($data);
	$data = $statuses->getAllStatuses(0, "twitterapi");
	var_dump(count($data));
	
	$users = new Users($twitter);
	$data = $users->getShowUser(0, "twitterapi");	
	var_dump($data);
	$data = $users->postLookupUsersByUserId(array('6253282', '783214'));
	var_dump($data);
	$data = $users->postLookupUsersByScreenName(array('twitterapi', 'twitter'));
	var_dump(@data);
	
	?>

Notes
-----
 
 Currently only application/rate_limit_status, statuses/user_timeline, users/show and users/lookup have been implemented.
 
TODO: implement more API calls!

Unit Testing
------------

Rename phpunit.xml.dist to phpunit.xml to set up unit testing, configure your Oauth parameters in the php section:

	<php>
		<const name="CONSUMER_KEY" value="consumer key goes here" />
		<const name="CONSUMER_SECRET" value="consumer secret goes here" />
		<const name="TOKEN" value="token goes here" />
		<const name="TOKEN_SECRET" value="token secret goes here" />
	</php>
	
To run mock tests only and ignore network tests, run: phpunit --exclude-group network
